autocmd Filetype cpp set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd Filetype javascript set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab

set autochdir
set autoindent
set number
set relativenumber 
set nowrap
set mouse+=a
set laststatus=2
set statusline=[%n]\ %<%f%h%m

